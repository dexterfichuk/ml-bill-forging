# README #

Bill Forge: Synthetic Bill Dataset Project

### Goals ###

* Create dataset of synthetic bills for potential release and/or retraining of Bill Pay app

### Subtasks ###

* Generate logos for fake companies using DCGAN
* Generate names for fake companies (using an RNN?)
* Analyze existing datset of real bills to find trends in template design
* Generate a template for each fake company, with varying font, colour scheme, box placement, etc., following insights from real bills
* Create n example bills for each fake company, with randomly generted account number, due amount and date
* Perform data augmentation on bills in various methods (using keras.preprocessing.image.ImageDataGenerator(), maybe also some Neural Style Transfer techniques)
